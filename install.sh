#!/usr/bin/bash

# pulled from https://github.com/joanisc/loyaltyCardsOpen/blob/master/install.sh
# which is a project I contribute to

path="${HOME}/.local/share"

mkdir -p "$path/elephant_remember"

if [ $? -ne 0 ]; then 
	echo ""
	echo "Could not create folder '$path/elephant_remember'"
	exit
fi

echo "" 
echo "Copying download folder to $path/elephant_remember"
cp -ru ./* "$path/elephant_remember"

if [ $? -ne 0 ]; then
	echo "" 
	echo "Could not copy download folder to $path/elephant_remember"
	exit
fi

echo "" 
echo "Create a new desktop file"
rm -f "$path/applications/elephant-remember.desktop"

echo "[Desktop Entry]
Name=Elephant Remember
X-GNOME-FullName=Elephant Remember
Comment=A reminder app built for Phosh and tested on Mobian
Keywords=Reminder;
Exec="$path/elephant_remember/reminder_app.sh"
Terminal=false
StartupNotify=true
Type=Application
Icon=$path/elephant_remember/Icons/grey_elephant.svg
Categories=GNOME;GTK;Utility;
MimeType=application/python;
Name[en_US]=Elephant Remember
X-Purism-FormFactor=Workstation;Mobile;" > "$path/applications/elephant-remember.desktop"

if [ $? -ne 0 ]; then 
	echo "" 
	echo "Could not create desktop file in to $path/applications"
	exit
fi

rm -f "$path/elephant_remember.sh"

echo "#!/usr/bin/bash
cd '$path/elephant_remember'
python3 reminder_app.py" > "$path/elephant_remember/reminder_app.sh"

chmod 0755 "$path/elephant_remember/reminder_app.sh"

if [ $? -ne 0 ]; then 
	echo "" 
	echo "Could not create shell script in to $path/elephant_remember"
	exit
fi

echo "" 
echo "Install complete - you may (optionally) remove the download folder"
