#!/usr/bin/python3
import gi
import os
import sys
from datetime import datetime
from dateutil.relativedelta import relativedelta
from icalendar import Calendar, Event, Alarm
import subprocess as sp

# grabbing username to get the path to calendar file
username = sp.check_output('whoami', shell=True)
username = username.decode('utf-8').strip('\n')

if isinstance(username, list):
    username = username[0]
else:
    username = username

"""
TODO:
 - Sort weekly reminders
 - Add recurring events, initial attempts are not promising...
"""


gi.require_version("Gtk", "3.0")

from gi.repository import Gtk, Gio, Gdk, GLib


cal_path=f"/home/{username}/.local/share/evolution/calendar/system/calendar.ics"

# class used to make the reminders page a listbox so you can scroll within it
class ListBoxRowWithData(Gtk.ListBoxRow):
    def __init__(self, data):
        super().__init__()
        self.data = data
        self.add(Gtk.Label(label=data))

# Class that pulls up the calendar dialog screen when setting reminders
class CalDialog(Gtk.Dialog):
    """
    Calendar Dialog
    """

    def __init__(self, parent):
        Gtk.Dialog.__init__(
            self,
            "Select Date",
            parent,
            0,
            (
                Gtk.STOCK_CANCEL,
                Gtk.ResponseType.CANCEL,
                Gtk.STOCK_OK,
                Gtk.ResponseType.OK,
            ),
        )

        self.set_default_size(300, 200)

        self.value = None

        box = self.get_content_area()

        calendar = Gtk.Calendar()
        calendar.set_detail_height_rows(1)
        calendar.set_property("show-details", True)
        calendar.set_detail_func(self.cal_entry)

        box.add(calendar)

        self.show_all()

    def cal_entry(self, calendar, year, month, date):
        self.value = calendar.get_date()


class reminder_app(Gtk.Window):
    """
    Basic reminder app for Mobian (maybe all phosh?) that syncs with
    gnome-calendar in the backend to make adding reminders easier in
    Phosh
    """

    def __init__(self):
        Gtk.Window.__init__(self, title="Elephant Reminder!")

        self.set_border_width(0)
        self.set_default_size(200, 400)  # Seems close to the right size

        # Notebook makes the screen tabbed
        self.notebook = Gtk.Notebook()
        self.notebook.set_vexpand(True)
        self.notebook.set_hexpand(True)
        self.add(self.notebook)

        # Intro page info!
        self.intro_page = Gtk.ScrolledWindow()
        self.intro_page.set_border_width(5)

        vscrollbar = Gtk.Scrollbar(orientation=Gtk.Orientation.VERTICAL)

        self.intro_label = Gtk.Label(label="This weeks reminders:\n")
        self.intro_label.props.halign = Gtk.Align.CENTER
        self.listbox_2 = Gtk.ListBox()

        # reads from the calendar to get reminders for the next week
        items = self.read_from_cal()
        self.listbox_2.add(self.intro_label)
        x = 0
        # lists through the items pulled and adds them to the
        # list of reminders displayed
        for item in items:
            self.listbox_2.add(ListBoxRowWithData(item))
            x += 1
            # This is so that when yu are list the reminders it creates an
            # empty space after each date so the screen doesn't look cluttered
            if x % 2 == 0:
                self.listbox_2.add(ListBoxRowWithData("\n"))

        def on_row_activated(listbox_widget, row):
            print(row.data)

        self.listbox_2.connect("row-activated", on_row_activated)

        self.intro_page.add(self.listbox_2)
        self.listbox_2.show_all()

        self.notebook.append_page(self.intro_page, Gtk.Label(label="Reminders"))

        # Reminder page info!
        self.reminder_page = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.add(self.reminder_page)

        # There are a few different list boxes here so that everything can
        # be added to the same screen without having to worry about sizing
        listbox = Gtk.ListBox()
        listbox.set_selection_mode(Gtk.SelectionMode.NONE)
        self.reminder_page.pack_start(listbox, True, True, 0)

        row = Gtk.ListBoxRow()
        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=50)
        row.add(hbox)
        hbox.set_border_width(5)
        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        hbox.pack_start(vbox, True, True, 0)


        label1 = Gtk.Label(label="What would you like to be reminded of?", xalign=0)
        label1.props.halign = Gtk.Align.CENTER
        vbox.pack_start(label1, True, True, 0)

        # textbox for the reminder entry
        self.reminder_entry = Gtk.Entry()
        self.reminder_entry.props.valign = Gtk.Align.CENTER
        vbox.pack_end(self.reminder_entry, True, True, 5)

        listbox.add(row)

        row = Gtk.ListBoxRow()
        grid = Gtk.Grid(orientation=Gtk.Orientation.HORIZONTAL)
        grid.props.halign = Gtk.Align.CENTER
        row.add(grid)
        grid.set_border_width(10)

        # calendar button and icon
        calendar_button = Gtk.Button()
        calendar_image = Gtk.Image()
        calendar_image.set_from_file(os.path.join(sys.path[0], "Icons", "calendar.svg"))
        calendar_button.add(calendar_image)
        calendar_button.connect("clicked", self.calendar_entry)
        
        # date stuff that populates the date entry with the current date and time
        year = datetime.now().year
        month = datetime.now().month
        day = datetime.now().day
        hour = datetime.now().strftime("%H")
        minute = datetime.now().strftime("%M")

        # date entry
        self.user_entry = Gtk.Entry()
        self.user_entry.set_text(f"{month}/{day}/{year}")
        
        # Hour Spinner, vertical so it looks better and follows the flow easier
        adjustments = Gtk.Adjustment(upper=23, step_increment=1)
        self.hour_spin = Gtk.SpinButton(orientation=Gtk.Orientation.VERTICAL)
        self.hour_spin.set_adjustment(adjustments)
        self.hour_spin.set_value(int(hour))

        # Minute Spinner
        adjustments = Gtk.Adjustment(upper=59, step_increment=15)
        self.minute_spin = Gtk.SpinButton(orientation=Gtk.Orientation.VERTICAL)
        self.minute_spin.set_adjustment(adjustments)
        self.minute_spin.set_value(int(minute))

        grid.attach(calendar_button, 0, 0, 1, 1)
        # Need to make this smaller
        grid.attach(self.user_entry, 1, 0, 1, 1)
        grid.attach(self.hour_spin, 2, 0, 1, 1)
        grid.attach(self.minute_spin, 3, 0, 1, 1)
        listbox.add(row)

        row = Gtk.ListBoxRow()
        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=50)
        row.add(hbox)
        hbox.set_border_width(10)
        # User feedback being the logo pops up and tells them what they
        # set a reminder for.
        save_event = Gtk.Button(label="Save Reminder!")
        save_event.connect("clicked", self.set_reminder)
        self.elephant_photo = Gtk.Image()
        # halign is horizontal and valign is vertical aligning
        save_event.props.halign = Gtk.Align.CENTER
        hbox.pack_start(save_event, True, True, 0)

        listbox.add(row)

        row = Gtk.ListBoxRow()
        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=50)
        row.add(hbox)
        hbox.set_border_width(10)
        # User feedback being the logo pops up and tells them what they
        # set a reminder for.

        self.elephant_photo = Gtk.Image()
        hbox.pack_start(self.elephant_photo, True, True, 0)
        listbox.add(row)

        row = Gtk.ListBoxRow()
        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=50)
        row.add(hbox)
        hbox.set_border_width(5)
        self.feedback_label = Gtk.Label()
        hbox.pack_end(self.feedback_label, True, False, 0)
        listbox.add(row)

        self.notebook.append_page(
            self.reminder_page, Gtk.Label(label="Create Reminder")
        )

        # Attaching everything to the grid, sorted from top to bottom
        # (button added, column, row, column span, row span)

        # adds the styling from the attached CSS file 'main.css'
        screen = Gdk.Screen.get_default()
        provider = Gtk.CssProvider()
        style_context = Gtk.StyleContext()
        style_context.add_provider_for_screen(
            screen, provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )
        # stuff needs to be lowercase for what it is in Gtk but without Gtk added in the name
        css_path = os.path.join(sys.path[0], "main.css")
        provider.load_from_path(css_path)

    def calendar_entry(self, widget):
        """
        What this will do is take the value and then
        put those values in a function that will make the reminder
        """
        dialog = CalDialog(self)
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            chosen_dates = dialog.value
            year = chosen_dates[0]
            month = chosen_dates[1] + 1
            day = chosen_dates[2]
            # This will grab the year, month and day. Month seems
            # to be behind by 1 though... So add one to that
        # Take dialog values and put them into the text box for
        # the cal_entry
        self.user_entry.set_text(f"{month}/{day}/{year}")

        dialog.destroy()

        
    def set_reminder(self, widget):
        event = Event()
        alarm = Alarm()
        # grab dates from the entry box
        dates = self.user_entry.get_text()
        # gets the text from the reminder box
        user_summary = self.reminder_entry.get_text()
        # splits the dates by the / so we can get just month, day and year
        dates = dates.split("/")
        # opens your calendar file
        g = open(cal_path, "rb")
        # reads your calendar file into the program
        gcal = Calendar.from_ical(g.read())
        # grabbing month, day and year from the entry
        months = dates[0]
        days = dates[1]
        years = dates[2]
        # gets start time from the spinners
        start_hour = self.hour_spin.get_value_as_int()
        start_minute = self.minute_spin.get_value_as_int()
        # adds an event summary which is grabbed from the entry box
        event.add("summary", f"{user_summary}")
        # date is year, month, day, hr, min, sec
        # keep tzinfo=None or else it throws tons of errors and wont work
        event.add(
            "dtstart",
            datetime(
                int(years),
                int(months),
                int(days),
                start_hour,
                start_minute,
                0,
                tzinfo=None,
            ),
        )
        event.add(
            "dtend",
            datetime(
                int(years),
                int(months),
                int(days),
                start_hour,
                start_minute,
                0,
                tzinfo=None,
            ),
        )
        event.add("priority", 5)
        # alarm is the reminder basically, set by default to the listed time
        alarm.add(
            "trigger",
            datetime(
                int(years),
                int(months),
                int(days),
                start_hour,
                start_minute,
                0,
                tzinfo=None,
            ),
        )
        alarm.add("action", "display")
        event.add_component(alarm)
        gcal.add_component(event)
        try:
            # writes the events to the calendar and saves it
            f = open(cal_path, "wb")
            f.write(gcal.to_ical())
            f.close()
            # clear the text box
            self.reminder_entry.set_text("")
            # user feedback for saving an event
            self.elephant_photo.set_from_file(
                os.path.join(sys.path[0], "Icons", "blue_elephant.svg")
            )
            self.feedback_label.set_text(f"Reminder set for:\n{user_summary}")
            self.feedback_label.set_line_wrap(True)
            #self.feedback_label.set_justify(Gtk.Justification.FILL)
            #self.notebook.set_current_page(0)
            
        except Exception as e:
            print(e)
        # updates the weekly review so the event can be seen immediately
        self.update_weekly_review()

            
    def read_from_cal(self):
        cal_path = f"/home/{username}/.local/share/evolution/calendar/system/calendar.ics"
        # opening and reading from the calendar
        g = open(cal_path, "rb")
        gcal = Calendar.from_ical(g.read())
        today = datetime.now()
        # empty list of events that gets populated with the weekly reminders
        events = []        
        next_week = today + relativedelta(weeks=1)
        for component in gcal.walk():
            if component.name == "VEVENT":
                summ = component.get("summary")
                start = component.get("dtstart").dt
                end = component.get("dtend").dt
                if isinstance(start, datetime):
                    start = start.replace(tzinfo=None)
                    if today <= start <= next_week:
                        events.append(summ)
                        events.append(start)

                else:
                    # some events are stored as a date
                    if today.date() <= start <= next_week.date():
                        events.append(summ)
                        events.append(start)
        return events

    # used to dynamically update the weekly reminders
    def update_weekly_review(self):
        items = self.read_from_cal()
        self.listbox_2.add(self.intro_label)
        x = 0
        for item in items:
            self.listbox_2.add(ListBoxRowWithData(item))
            x += 1
            if x % 2 == 0:
                self.listbox_2.add(ListBoxRowWithData("\n"))

        self.intro_page.add(self.listbox_2)
        self.listbox_2.show_all()

if __name__ == "__main__":
    window = reminder_app()
    window.connect("destroy", Gtk.main_quit)
    window.show_all()
    Gtk.main()
