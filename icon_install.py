#!/usr/bin/python3
import os
import subprocess as sp

username = sp.check_output('whoami', shell=True)
username = username.decode('utf-8').strip('\n')

if isinstance(username, list):
    username = username[0]
else:
    username = username

script_path = os.path.dirname(os.path.realpath(__file__))
path = f"{script_path}/reminder_app.py"
install_path = f"/home/{username}/.local/share/applications/elephant_remember.desktop"
logo_path = f"{script_path}/Icons/grey_elephant.svg"

icon_file = f"""[Desktop Entry]
Name=Elephant Remember
Comment=Reminder app for Phosh on the PinePhone
Type=Application
Icon={logo_path}
Exec={path}
Categories=Application
"""

with open(install_path, "w+") as f:
    f.write(icon_file)
